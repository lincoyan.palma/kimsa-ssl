#!/bin/bash
DOMAINS=
FIRSTDOMAIN=$1
for DOMAIN in "$@"
do
 DOMAINS="$DOMAINS  --domains $DOMAIN "
done
echo $DOMAINS


echo "Instalando Lego Client..."
cd /tmp
wget https://github.com/go-acme/lego/releases/download/v2.4.0/lego_v2.4.0_linux_amd64.tar.gz 2>/dev/null
tar xf lego_v2.4.0_linux_amd64.tar.gz
sudo mv lego /usr/local/bin/lego
echo "Lego Client OK, deteniendo servicios Bitnami..."
sudo /opt/bitnami/ctlscript.sh stop
echo "Servicio detenido, buscando certificados..."
sudo lego --tls --email="lincoyanpalma@webcompu.cl" $DOMAINS --path="/etc/lego" run

echo "Configurando Apache..."
sudo mv /opt/bitnami/apache2/conf/server.crt /opt/bitnami/apache2/conf/server.crt.old
sudo mv /opt/bitnami/apache2/conf/server.key /opt/bitnami/apache2/conf/server.key.old
sudo mv /opt/bitnami/apache2/conf/server.csr /opt/bitnami/apache2/conf/server.csr.old
sudo ln -s /etc/lego/certificates/$FIRSTDOMAIN.key /opt/bitnami/apache2/conf/server.key
sudo ln -s /etc/lego/certificates/$FIRSTDOMAIN.crt /opt/bitnami/apache2/conf/server.crt
sudo chown root:root /opt/bitnami/apache2/conf/server*
sudo chmod 600 /opt/bitnami/apache2/conf/server*

echo "Iniciado servicios Bitnami..."
sudo /opt/bitnami/ctlscript.sh start

echo "Configurando redireccionamiento HTTPS…"
sed "/<VirtualHost _default_:80>/ a  RewriteEngine On" /opt/bitnami/apache2/conf/bitnami/bitnami.conf > changed.txt && mv changed.txt /opt/bitnami/apache2/conf/bitnami/bitnami.conf

sed "/RewriteEngine On/ a  RewriteCond %{HTTPS} !=on" /opt/bitnami/apache2/conf/bitnami/bitnami.conf > changed.txt && mv changed.txt /opt/bitnami/apache2/conf/bitnami/bitnami.conf

sed "/RewriteEngine On/ a  RewriteRule ^/(.*) https://$FIRSTDOMAIN/\$1 [R,L]" /opt/bitnami/apache2/conf/bitnami/bitnami.conf > changed.txt && mv changed.txt /opt/bitnami/apache2/conf/bitnami/bitnami.conf

sudo /opt/bitnami/ctlscript.sh restart apache

echo "Creando script de renovación..."
RENEWFILE=/etc/lego/renew-certificate.sh
cat <<END > $RENEWFILE
#!/bin/bash

sudo /opt/bitnami/ctlscript.sh stop; sudo lego --tls --email="lincoyanpalma@webcompu.cl" $DOMAINS --path="/etc/lego" renew; sudo /opt/bitnami/ctlscript.sh start;
END

chmod +x /etc/lego/renew-certificate.sh
echo "Programando tarea de renovacion..."
CRONTABFILE=/home/bitnami/crontab
cat <<END > $CRONTABFILE
0 0 1 * * /etc/lego/renew-certificate.sh 2> /dev/null
END

crontab $CRONTABFILE
echo "Tareas finalizadas!"

